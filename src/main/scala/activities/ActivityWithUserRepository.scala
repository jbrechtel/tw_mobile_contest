package org.tw.atl.contest

import android.content.Context

trait ActivityWithUserRepository { this: Context =>
  val optionUserRepo: Option[UserRepository]

  lazy val userRepo = optionUserRepo match {
    case Some(repo) => repo
    case None       => new FileSystemUserRepository(getFileStreamPath("user.json"))
  }

}
