package org.tw.atl.contest

import android.app.ListActivity
import android.app.ProgressDialog
import android.os.Bundle
import android.content.Context
import android.view.LayoutInflater
import android.widget.ListView
import android.view.View
import android.widget.Toast

class JoinGameActivity(gameService: GameService) extends ListActivity with AndroidHelpers {
  def this() = this(new RestGameService(Constants.productionUrl))

  lazy val layoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE).asInstanceOf[LayoutInflater]

	override def onCreate(savedInstanceState: Bundle) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.join)


    val dialog = ProgressDialog.show(this, "", "Loading games....", false)
    asyncFunction(gameService.games) { games =>
      val adapter = new GameListAdapter(games, layoutInflater)
      setListAdapter(adapter)
      dialog.dismiss()
    }
	}

  override def onListItemClick(listView: ListView, view: View, position: Int, id: Long) {
    val gameId = listView.getAdapter.getItem(position).asInstanceOf[Game].id

    val dialog = ProgressDialog.show(this, "", "Joining game...", false)
    asyncFunction(gameService.joinGame(gameId, currentUser.id)) { nothing =>
      Toast.makeText(this, R.string.joined_game, Toast.LENGTH_LONG).show()
      dialog.dismiss()
      finish()
    }
  }

}
