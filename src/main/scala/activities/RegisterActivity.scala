package org.tw.atl.contest

import android.app.Activity
import android.os.Bundle
import android.bluetooth._
import android.content._
import android.widget._
import android.view._


class RegisterActivity(gameService: GameService, val optionUserRepo: Option[UserRepository])
  extends Activity
  with AndroidAsync
  with AndroidHelpers
  with ActivityWithUserRepository {

  def this() = this(new RestGameService(Constants.productionUrl), None)

  lazy val registerButton = findViewById(R.id.register_button).asInstanceOf[Button]
  lazy val usernameTextView = findViewById(R.id.username_textview).asInstanceOf[TextView]
  def bluetoothAddress = {
    if(BluetoothAdapter.getDefaultAdapter == null) "" else BluetoothAdapter.getDefaultAdapter.getAddress
  }

  override def onCreate(bundle: Bundle) {
    super.onCreate(bundle)
    setContentView(R.layout.register)
    registerButton.setOnClickListener(() => registerUser)
  }

  def registerUser() {
    val username = usernameTextView.getText.toString
    asyncFunction(gameService.registerUser(username, bluetoothAddress, "")) { user =>
      userRepo.set(user)
      C2DM.register(this)
      android.widget.Toast.makeText(this, R.string.registered, Toast.LENGTH_LONG).show()
      finish()
    }
  }
}
