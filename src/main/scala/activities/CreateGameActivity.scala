package org.tw.atl.contest

import android.app.Activity
import android.os.Bundle
import android.bluetooth._
import android.content._
import android.widget._
import android.view._

class CreateGameActivity(gameService: GameService) extends Activity with AndroidHelpers {
  def this() = this(new RestGameService(Constants.productionUrl))

  lazy val titleTextView = findViewById(R.id.title_textview).asInstanceOf[TextView]
  lazy val createGameButton = findViewById(R.id.create_game_button).asInstanceOf[Button]

  override def onCreate(bundle: Bundle) {
    super.onCreate(bundle)
    setContentView(R.layout.create_game)

    createGameButton.setOnClickListener(() => createGame)
  }

  def createGame() {
    val title = titleTextView.getText.toString
    asyncFunction(gameService.createGame(title, currentUser)) { game =>
      Toast.makeText(this, R.string.created_game, Toast.LENGTH_LONG).show()
      finish()
    }
  }

}

