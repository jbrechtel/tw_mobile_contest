package org.tw.atl.contest

import android.app.Activity
import android.os.Bundle
import android.bluetooth._
import android.content._
import android.widget._
import android.view._

class MainActivity(val optionUserRepo: Option[UserRepository])
  extends Activity
  with AndroidHelpers
  with ActivityWithUserRepository {

  def this() = this(None)

  lazy val joinGameButton = findViewById(R.id.launch_join_game_button).asInstanceOf[Button]
  lazy val createGameButton = findViewById(R.id.launch_create_game_button).asInstanceOf[Button]
  lazy val c2dmButton = findViewById(R.id.register_c2dm).asInstanceOf[Button]

  override def onCreate(bundle: Bundle) {
    super.onCreate(bundle)
    setContentView(R.layout.main)

    joinGameButton.setOnClickListener(() => startUserActivity[JoinGameActivity])
    createGameButton.setOnClickListener(() => startUserActivity[CreateGameActivity])
    c2dmButton.setOnClickListener(() => C2DM.register(this))
  }

  override def onResume() {
    super.onResume()
    asyncOperation {
      if(userRepo.get == None) startActivity[RegisterActivity]
    }
  }

  override def currentUser = userRepo.get.get
}
