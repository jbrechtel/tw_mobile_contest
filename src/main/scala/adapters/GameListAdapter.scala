package org.tw.atl.contest

import android.widget.BaseAdapter
import android.widget.TextView
import android.view.ViewGroup
import android.view.View
import android.view.LayoutInflater

class GameListAdapter(games: Seq[Game], layoutInflater: LayoutInflater) extends BaseAdapter {
	def getCount = games.size

	def getItem(index: Int) = games(index)

	def getItemId(index: Int) = index

	def getView(index: Int, convertView: View, parent: ViewGroup): View = {
		val view = if(convertView == null)
                layoutInflater.inflate(R.layout.game_view, null, false)
               else
                convertView

    view.findViewById(R.id.game_title_textview).asInstanceOf[TextView].setText(games(index).title)
    view
	}

}
