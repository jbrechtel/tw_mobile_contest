package org.tw.atl.contest

trait GameService {
  def registerUser(username: String, bluetoothAddress: String, c2dmId: String): User
  def createGame(title: String, user: User): Game
  def games: Seq[Game]
  def joinGame(gameId: String, userId: String)
  def registerForPush(user: User, c2dmId: String): User
}
