package org.tw.atl.contest

import us.monoid.web.Resty._
import us.monoid.web.Resty
import cc.spray.json._

class RestGameService(baseUrl: String) extends GameService with BTAJsonProtocol {

  val usersUrl = baseUrl+"/users"
  val userUrl = baseUrl+"/user"
  val gamesUrl = baseUrl+"/games"
  val gameUrl = baseUrl+"/game"

  val client = new Resty()

  def registerUser(username: String, bluetoothAddress: String, c2dmId: String) = {
    val formData = form(data("name", username),
                        data("address", bluetoothAddress),
                        data("c2dm_id", c2dmId))
    val result = client.text(usersUrl, formData).toString.asJson
    userFormat.read(result)
  }

  def createGame(title: String, creator: User) = {
    val formData = form(data("title", title), data("creator_id", creator.id), data("creator_name", creator.name))
    val result = client.text(gamesUrl, formData).toString.asJson
    gameFormat.read(result)
  }

  def games = {
    val result = client.text(gamesUrl).toString.asJson
    listFormat[Game].read(result)
  }

  def joinGame(gameId: String, userId: String) = {
    val registrationUrl = gameUrl + "/" + gameId + "/registrations"
    val formData = form(data("user_id", userId))
    val result = client.text(registrationUrl, formData).toString.asJson
    registrationFormat.read(result)
  }

  def registerForPush(user: User, c2dmId: String) = {
    val url = userUrl + "/" + user.id + "/" + c2dmId
    client.text(url, put(content("")))
    user.copy(c2dmId=c2dmId)
  }
}

