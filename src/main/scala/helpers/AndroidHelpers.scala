package org.tw.atl.contest

import android.content.Context
import android.view.View
import android.app.Activity
import android.content.Intent

trait AndroidHelpers extends AndroidAsync { this: Activity =>

  implicit def func2OnClickListener[A,B](func: () => B): View.OnClickListener = {
    new View.OnClickListener() {
      def onClick(view: View) {
        func()
      }
    }
  }

  def startUserActivity[A <: Activity : Manifest]: Unit = {
    val intent = new Intent(this, manifest[A].erasure)
    userMap.foreach { case (key, value) =>
      intent.putExtra(key,value)
    }
    startActivity(intent)
  }

  def startActivity[A <: Activity : Manifest]: Unit = {
    val intent = new Intent(this, manifest[A].erasure)
    startActivity(intent)
  }

  def currentUser = {
    val userId = getIntent.getStringExtra("userId")
    val userName = getIntent.getStringExtra("userName")
    val bluetoothAddress = getIntent.getStringExtra("bluetoothAddress")
    val c2dmId = getIntent.getStringExtra("c2dmId")
    User(userId,userName,bluetoothAddress,c2dmId)
  }

  def userMap = Map("userId" -> currentUser.id,
                    "userName" -> currentUser.name,
                    "c2dmId" -> currentUser.c2dmId,
                    "bluetoothAddress" -> currentUser.bluetoothAddress)
}
