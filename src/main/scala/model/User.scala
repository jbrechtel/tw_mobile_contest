package org.tw.atl.contest

case class User(id: String, name: String, bluetoothAddress: String, c2dmId: String)
case class Registration(id: String)
