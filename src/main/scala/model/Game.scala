package org.tw.atl.contest

case class Game(id: String, title: String, creatorId: String, creatorName: String)
