package org.tw.atl.contest

import android.os.AsyncTask

trait AndroidAsync {
  def asyncOperation[T](oper: => T) {
    new AsyncTask[Object,  Object,  T] {
      def doInBackground(junk: Object*) = {
      	try { oper }
      	catch { case e: Exception => android.util.Log.d("ERROR", e.toString); throw e; }
      }

    }.execute()
  }

  def asyncFunction[T](innerFunc: => T)(outerFunc: T => Any) {
    new AsyncTask[Object,  Object,  T] {
      def doInBackground(junk: Object*) = {
      	try { innerFunc }
      	catch { case e: Exception => android.util.Log.d("ERROR", e.toString); throw e; }
      }

      override def onPostExecute(result: T) {
        outerFunc(result)
      }
    }.execute()
  }

}
