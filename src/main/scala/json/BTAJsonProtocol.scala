package org.tw.atl.contest

import cc.spray.json.{JsonFormat, DefaultJsonProtocol}

trait BTAJsonProtocol extends DefaultJsonProtocol {
  implicit val userFormat = jsonFormat(User, "uuid", "name", "address", "c2dm_id")
  implicit val gameFormat = jsonFormat(Game, "_id", "title", "creator_id", "creator_name")
  implicit val registrationFormat = jsonFormat(Registration, "_id")
}
