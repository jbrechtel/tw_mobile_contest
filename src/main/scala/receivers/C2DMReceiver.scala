package org.tw.atl.contest

import com.google.android.c2dm._
import android.widget.Toast
import android.content._

class C2DMReceiver extends C2DMBaseReceiver(C2DM.sender) {
  val gameService = new RestGameService(Constants.productionUrl)
  lazy val userRepo = new FileSystemUserRepository(getFileStreamPath("user.json"))

  override def onError(context: Context, errorId: String) {
    android.util.Log.d("BTA", "error:"+errorId)
    Toast.makeText(context, "Registration error: " + errorId, Toast.LENGTH_LONG).show()
  }

  override def onMessage(context: Context, intent: Intent) {
    android.util.Log.d("BTA", "message!")
    Toast.makeText(context, "Got message!", Toast.LENGTH_LONG).show()
  }

  override def onRegistered(context: Context, registrationId: String) {
    //call push service and send our registrationId along with our userId
    //store our registrationId with our userId
    val user = userRepo.get.get
    android.util.Log.d("BTA", "registered:"+registrationId)
    android.util.Log.d("BTA", "user for c2dm: "+user.id)
    userRepo.set(gameService.registerForPush(user, registrationId))
  }

}

object C2DM {
  def sender = "bluetoothassassin@gmail.com"
  def register(context: Context) {
    C2DMessaging.register(context, sender)
  }
}
