package org.tw.atl.contest

import java.util.Scanner
import java.io._
import scala.collection.JavaConversions._
import cc.spray.json._

class FileSystemUserRepository(userFile: File) extends UserRepository with BTAJsonProtocol {
  def get: Option[User] = {
    userFile.exists match {
      case true  => readUserFile
      case false => None
    }
  }

  def set(user: User) {
    val writer = new FileWriter(userFile)
    val bufferedWriter = new BufferedWriter(writer)
    bufferedWriter.write(userFormat.write(user).toString)
    bufferedWriter.close()
  }

  private def readUserFile = {
    val scanner = new Scanner(userFile)
    Some(userFormat.read(scanner.toList.mkString("\n").asJson))
  }
}
