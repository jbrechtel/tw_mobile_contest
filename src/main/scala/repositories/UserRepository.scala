package org.tw.atl.contest

trait UserRepository {
  def get: Option[User]
  def set(user: User)
}
