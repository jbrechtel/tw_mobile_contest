package org.tw.atl.contest

class StubUserRepository(user: User) extends UserRepository {
  def get = Some(user)
  def set(user: User) {}
}
