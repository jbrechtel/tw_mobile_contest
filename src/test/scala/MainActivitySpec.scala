import org.specs2.mutable._
import org.specs2.mock._
import org.tw.atl.contest._
import com.github.jbrechtel.robospecs.RoboSpecs
import android.widget._
import android.view.View
import com.xtremelabs.robolectric.Robolectric.shadowOf

class MainActivitySpec extends RoboSpecs with Mockito {
  "MainActivity" should {
    "launch the register activity if the user is not logged in" in {
      val activity = new MainActivity(None)
      activity.onCreate(null)
      activity.onResume()
      val nextActivity = shadowOf(activity).getNextStartedActivity
      nextActivity.getComponent.getClassName must be(classOf[RegisterActivity].getCanonicalName)
    }

    "launch the create game activity" in {
      val activity = new MainActivity(Some(new StubUserRepository(User("","",""))))
      activity.onCreate(null)
      activity.findViewById(R.id.launch_create_game_button).asInstanceOf[Button].performClick()
      val nextActivity = shadowOf(activity).getNextStartedActivity
      nextActivity.getComponent.getClassName must be(classOf[CreateGameActivity].getCanonicalName)
    }

    "launch the join game activity" in {
      val activity = new MainActivity(Some(new StubUserRepository(User("","",""))))
      activity.onCreate(null)
      activity.findViewById(R.id.launch_join_game_button).asInstanceOf[Button].performClick()
      val nextActivity = shadowOf(activity).getNextStartedActivity
      nextActivity.getComponent.getClassName must be(classOf[JoinGameActivity].getCanonicalName)
    }

    "define currentUser based on the given userRepository" in {
      val expectedUser = User("123", "billybob", "99:99:99")
      val activity = new MainActivity(Some(new StubUserRepository(expectedUser)))
      activity.currentUser must beEqualTo(expectedUser)
    }
  }
}

