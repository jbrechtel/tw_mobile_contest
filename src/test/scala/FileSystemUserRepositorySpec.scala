import org.specs2.mutable._
import org.specs2.mock._
import org.tw.atl.contest._
import java.io._
import com.github.jbrechtel.robospecs.RoboSpecs

class FileSystemUserRepositorySpec extends RoboSpecs with Mockito with BTAJsonProtocol {
  "FileSystemUserRepository" should {
    "return no user when the file given doesn't exist" in {
      val repo = new FileSystemUserRepository(new File("/some/file/that/doesnt/exist"))
      repo.get must beEqualTo(None)
    }

    "return the user represented by the temp file when it exists" in {
      val userFile = File.createTempFile("userFile", "tests")
      userFile.deleteOnExit()

      val user = User("123", "joebob", "23:12:12:12")
      val userJson = userFormat.write(user).toString
      writeToDisk(userFile, userJson)

      val repo = new FileSystemUserRepository(userFile)
      repo.get must beEqualTo(Some(user))
    }

    "return the from 'get' the user written with 'set'" in {
      val userFile = File.createTempFile("userFile", "tests")
      userFile.deleteOnExit()

      val repo1 = new FileSystemUserRepository(userFile)
      val repo2 = new FileSystemUserRepository(userFile)

      val user = User("123", "billybob", "99:99:99")
      repo1.set(user)
      repo2.get must beEqualTo(Some(user))
    }
  }


  def writeToDisk(destination: File, contents: String) {
    val writer = new FileWriter(destination)
    val bufferedWriter = new BufferedWriter(writer)
    bufferedWriter.write(contents)
    bufferedWriter.close()
  }
}
