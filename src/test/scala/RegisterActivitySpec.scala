import org.specs2.mutable._
import org.specs2.mock._
import org.tw.atl.contest._
import com.github.jbrechtel.robospecs.RoboSpecs
import android.widget._

class RegisterActivitySpec extends RoboSpecs with Mockito {
  def exactly[T](item: T) = org.mockito.Matchers.eq(item)

  "RegisterActivity" should {
    "registering should call gameservice" in {
      val mockGameService = mock[GameService]
      val activity = new RegisterActivity(mockGameService,
                                          Some(new StubUserRepository(User("","",""))))

      activity.onCreate(null)
      val username = activity.findViewById(R.id.username_textview).asInstanceOf[TextView]
      username.setText("someguy")
      val button = activity.findViewById(R.id.register_button).asInstanceOf[Button]
      button.performClick()
      there was one(mockGameService).registerUser(exactly("someguy"), anyString)
    }

    "registering should call gameservice" in {
      val mockUserRepository = mock[UserRepository]
      val expectedUser = User("123","sasha","55:55:55")
      val activity = new RegisterActivity(new StubGameService(Nil, expectedUser),
                                          Some(mockUserRepository))

      activity.onCreate(null)
      val button = activity.findViewById(R.id.register_button).asInstanceOf[Button]
      button.performClick()
      there was one(mockUserRepository).set(expectedUser)
    }
  }
}

