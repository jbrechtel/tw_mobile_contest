import org.specs2.mutable._
import org.specs2.mock._
import org.tw.atl.contest._
import com.github.jbrechtel.robospecs.RoboSpecs
import android.widget._
import android.view.View
import android.widget.ListView
import com.xtremelabs.robolectric.Robolectric.shadowOf

class JoinGameActivitySpec extends RoboSpecs with Mockito {
  "JoinGameActivity" should {
    "join a game when clicking on one" in {
      val mockGameService = mock[GameService]
      val games = List(Game("wrong","","",""), Game("expectedGameId","","",""))
      mockGameService.games returns games

      val activity = new JoinGameActivity(mockGameService) {
        override def currentUser = User("expectedUserId", "joeuser", "123123")
      }

      activity.onCreate(null)
      activity.onListItemClick(activity.getListView, mock[View], 1, -1)

      there was one(mockGameService).joinGame("expectedGameId", "expectedUserId")
    }
  }
}

