package org.tw.atl.contest

class StubGameService(allGames: Seq[Game], registeredUser: User) extends GameService {
  def this(allGames: Seq[Game]) = this(allGames, User("fakeId", "", ""))

  def games = games

  def registerUser(name: String, bluetoothAddress: String) = registeredUser

  def createGame(title: String, creator: User) = Game("fakeId", title, creator.id, creator.name)

  def joinGame(gameId: String, userId: String) {}

}
