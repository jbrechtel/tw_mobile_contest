import org.specs2.mutable._
import org.specs2.mock._
import org.tw.atl.contest._
import com.github.jbrechtel.robospecs.RoboSpecs
import android.widget._
import com.xtremelabs.robolectric.shadows.ShadowLayoutInflater
import com.xtremelabs.robolectric.Robolectric

class GameListAdapterActivitySpec extends RoboSpecs with Mockito {
  "GameListAdapter" should {
    "create a view with the game's name" in {
      val layoutInflater = ShadowLayoutInflater.from(Robolectric.application)
      val oneGame = List(Game("123", "some game", "333", "joe"))
      val adapter = new GameListAdapter(oneGame, layoutInflater)
      val view = adapter.getView(0, null, null)
      view.findViewById(R.id.game_title_textview).asInstanceOf[TextView].getText.toString mustEqual "some game"
    }
  }
}


