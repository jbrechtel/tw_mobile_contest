import sbt._

import Keys._
import AndroidKeys._

object General {
  val settings = Defaults.defaultSettings ++ Seq (
    name := "Bluetooth Assassin",
    version := "0.1",
    versionCode := 0,
    scalaVersion := "2.9.2",
    parallelExecution in Test := false,
    testOptions in Test += Tests.Argument("sequential"),
    platformName in Android := "android-10",
    resolvers ++= Seq(roboSpecsRepo, sprayRepo),
    libraryDependencies ++= Seq(roboSpecs, resty, sprayJson)

  )

  val resty = "us.monoid.web" % "resty" % "0.3.1"

  val roboSpecs = "com.github.jbrechtel" %% "robospecs" % "0.2" % "test"
  val roboSpecsRepo = "jbrechtel github" at "http://jbrechtel.github.com/repo/releases/"

  val sprayJson = "cc.spray" %% "spray-json" % "1.1.1"
  val sprayRepo = "spray" at "http://repo.spray.cc/"

  val proguardSettings = Seq (
    useProguard in Android := true
  )

  lazy val fullAndroidSettings =
    General.settings ++
    AndroidProject.androidSettings ++
    proguardSettings ++
    AndroidManifestGenerator.settings ++
    AndroidMarketPublish.settings ++ Seq (
      keyalias in Android := "change-me"
    )
}

object AndroidBuild extends Build {
  lazy val main = Project (
    "Bluetooth Assassin",
    file("."),
    settings = General.fullAndroidSettings
  )

  lazy val tests = Project (
    "tests",
    file("tests"),
    settings = General.settings ++
               AndroidTest.settings ++
               General.proguardSettings ++ Seq (
      name := "BluetoothAssassinTests"
    )
  ) dependsOn main
}
